
require s7plc, 1.4.1
require modbus, 3.2.0

epicsEnvSet("PLC_IP", "172.30.150.83")

< IOC_S7PLC_CONFIGURATION.cmd
< IOC_MODBUS_CONFIGURATION.cmd

dbLoadRecords("iocEss_nblm.db")

